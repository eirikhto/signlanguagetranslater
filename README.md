# Sign Language Translater

This single page application translates text into sign language. It requires a user login, and stores the translations to an API. It also display the 10 last translations on a profile page.

## Dependencies
react router
react redux
react forms


## Contributors
Only me, Eirik Torp. With good help from Noroff Accelerate learning resources.

