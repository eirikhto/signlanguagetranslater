import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Navbar from './components/Navbar';
import LoginPage from './views/LoginPage';
import ProfilePage from './views/ProfilePage';
import TranslationPage from './views/TranslationPage';

function App() {

  return (
    <BrowserRouter>
      <div className="App">
        <Navbar/>
        <Routes>
          <Route path="/" element={<LoginPage/>}/>
          <Route path="/translation" element={<TranslationPage/>}/>
          <Route path="/Profile" element={<ProfilePage/>}/>
        </Routes>
      </div>
    </BrowserRouter>

  );
}

export default App;
