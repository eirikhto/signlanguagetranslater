import LoginForm from "../components/Login/LoginForm"
import whenLoggedIn from "../components/whenLoggedIn"

const LoginPage = () => {
    return (
        <>
            <h1>Login</h1>
            <LoginForm/>
        </>
    )
}
export default whenLoggedIn(LoginPage)