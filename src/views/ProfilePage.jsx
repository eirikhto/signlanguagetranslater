import DisplayRecentTranslations from "../components/Profile/DisplayRecentTranslations"
import withAuth from "../components/withAuth"

const ProfilePage = () => {
    return (
        <>
            <h1>Profile</h1>
            <DisplayRecentTranslations/>
        </>
        
    )
}
export default withAuth(ProfilePage)