import TranslationInput from "../components/Translation/TranslationInput"
import withAuth from "../components/withAuth"

const TranslationPage = () => {
    return (
        <>
            <h1>Translation</h1>
            <TranslationInput/>
        </>
        
    )
}
export default withAuth(TranslationPage)