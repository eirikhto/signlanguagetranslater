
//Displays text recieved from parent through props
const DisplayTranslationText = (props) => {
    return (
        <p>{props.text}</p>
    )
}

export default DisplayTranslationText