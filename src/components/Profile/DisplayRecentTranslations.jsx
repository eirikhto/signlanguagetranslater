import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { deleteTranslations } from "../../api/deleteTranslations"
import { setUser } from "../../redux-parts/userSlice"
import DisplayTranslationText from "./DisplayTranslationText"

//displays the 10 recent translations as well as a button to delete them all
const DisplayRecentTranslations = () => {
    //react redux, global state
    const user = useSelector(state => state.user.value)
    const dispatch = useDispatch()

    //local state to disable user input during api requests
    const [apiLoading, setApiLoading] = useState(false)

    //local array of last translations
    const [lastTranslations, setLastTranslations] = useState([])

    //creates array of 10 last translations (or all translations if less than 10)
    if (lastTranslations.length !== user.translations.length) {
        if (user.translations.length > 10) {
            setLastTranslations(user.translations.slice(-10))
        }
        else {
            setLastTranslations(user.translations)
        }
    
    }
   
    const handleDeleteClick = async () => {
        
        //make sure user wants to delete
        if(!window.confirm("Are you sure you want to delete the translations?")) {return}

        setApiLoading(true)

        //deletes translations from api and updates store and local storage if successful 
        const [data, errorMsg] = await deleteTranslations(user)
        if (errorMsg != null) {
            console.log(errorMsg)
            window.alert("Could not delete translations. Please try again later.")
        }
        else {
            window.localStorage.setItem("username", JSON.stringify(data))
            dispatch(setUser(data))
        }

        setApiLoading(false)
    }

    return (
        <>
            <h3>Your last translations (max. 10):</h3>
            {lastTranslations.map((elem, i) => <DisplayTranslationText key={i} text={elem}/>)}

            {/* During api request: button is diabled and paragraph is displayed */}
            <button onClick={handleDeleteClick} disabled={apiLoading}>Delete Translations</button>
            {apiLoading && <p>Deleting translations...</p>}
        </>
        
        
    )
}

export default DisplayRecentTranslations