import { useState } from "react"
import { useForm } from "react-hook-form"
import { useDispatch } from "react-redux"
import { useNavigate } from "react-router-dom"
import { loginUser } from "../../api/loginUser"
import { setUser } from "../../redux-parts/userSlice"


//specifying requirements for user input in loginform
const usernameConfig = {
    required: true,
    minLength: 2,
    maxLength: 30
}

//Handles user login
const LoginForm = () => {
    //react redux
    const dispatch = useDispatch()

    //routing
    const navigate = useNavigate()

    //react forms
    const {register, handleSubmit, formState: { errors }} = useForm()

    //local state. used to block user input during api requests
    const [apiLoading, setApiLoading] = useState(false)

    //handles username submission
    const onSubmit = async ({username}) => {
        setApiLoading(true)

        const [data, errorMsg] = await loginUser(username)
        if (errorMsg != null) {
            //Some error occured. letting use know
            window.alert("Could not log in. Please try again later")
            console.log(errorMsg)
            setApiLoading(false)
            return
        }

        //store user data in store and local storage
        dispatch(setUser(data))
        window.localStorage.setItem("username", JSON.stringify(data))

        //redirect to translation page
        navigate("translation")

        setApiLoading(false)
    }

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)}>
                <fieldset>
                    <label htmlFor="username">Please enter username: </label>
                    <input type="text" placeholder="E.g. olaNordmann" {...register("username", usernameConfig)}/>
                    {/* Displaying text to inform the user if the input doesn't meet the requirements */}
                    {errors.username && (
                        (errors.username.type === "required" && <span>Please enter username</span>) ||
                        (errors.username.type === "minLength" && <span>username too short</span>) ||
                        (errors.username.type === "maxLength" && <span>username too long</span>)
                    )}
                </fieldset>
                
                {/* Button is disabled during API requests */}
                <button type="submit" disabled={apiLoading}>Login</button>
                
                {/* Only showa during api requests */}
                {apiLoading && <p>Logging in...</p>
}
            </form>
        </>
    )
}

export default LoginForm