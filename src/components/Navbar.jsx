import { useDispatch, useSelector } from "react-redux"
import { NavLink } from "react-router-dom"
import { setUser } from "../redux-parts/userSlice"

const Navbar = () => {
    //global state
    const dispatch = useDispatch()
    const user = useSelector(state => state.user.value)

    //logout function deletes user data in store and local storage
    //user is redirected to login page upon rerendering because of the withAuth guard
    const logout = () => {
        if (window.confirm("Are you sure you want to log out?")) {
            window.localStorage.removeItem("username")
            dispatch(setUser(null))
        }
    }

    return (
        <>
            {/* only shows content if user is logged in */}
            {user !== null &&
                <>
                    <nav>
                        <li><NavLink to="/translation">Translation</NavLink></li>
                        <li><NavLink to="/Profile">Profile</NavLink></li>
                    </nav>
                    <button onClick={logout}>Logout</button>
                </>
            }
        </>
    )
}

export default Navbar