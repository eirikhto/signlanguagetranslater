import { Navigate } from "react-router-dom"

//guard spesifically for login page.
//redirects to translation page if user is logged in
const whenLoggedIn = Component => props => {
    //only renders Component if user is not logged in
    if (window.localStorage.getItem("username") === null) {
        return <Component {...props}/>
    }
    else {
        return <Navigate to="translation"/>
    }
}

export default whenLoggedIn