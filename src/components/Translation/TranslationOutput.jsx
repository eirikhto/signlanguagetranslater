/* Takes in an array of chars through props and maps the array to sign language images */
const TranslationOutput = (props) => {

    return (
        <>  
            <h2>Translation output</h2>
            <p><i>Only english letters (A-Z and a-z) will be translated. 
                Whitespace and special characters will be ignored. </i></p>
            {/* Checks if array is empty before calling map function */}
            {props.data.length !== 0 && props.data.map((char, i) => 
                <img key={i} style={{width: "50px"}} src={"img/individual_signs/" + char + ".png"} alt={char}/>)}
        </>
        
    )
}

export default TranslationOutput