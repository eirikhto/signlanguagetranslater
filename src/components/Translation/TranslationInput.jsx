import { useState } from "react"
import {useForm} from "react-hook-form"
import { useDispatch, useSelector } from "react-redux"
import { addTranslationToAPI } from "../../api/addTranslationToAPI"
import { setUser } from "../../redux-parts/userSlice"
import TranslationOutput from "./TranslationOutput"


const TranslationInput = () => {
    //react redux
    const user = useSelector(state => state.user.value)
    const dispatch = useDispatch()
    
    //local state. makes sure sign language output is rerendered upon new input
    const [toBeTranslated, setToBeTranslated] = useState([])

    //local state. makes sure submit button is disabled during api requests
    const [apiLoading, setApiLoading] = useState(false)

    //react forms
    const {register, handleSubmit, formState: {errors}} = useForm()

    //stores input (if valid) to API. 
    //updates local state with array of chars to be translated
    const onSubmit = async ({rawInput}) => {
        setApiLoading(true)

        //convert input string to array of lowercase english letters
        //whitespace and special characters are ignored
        const charsToBeTranslated = rawInput.toLowerCase().match(/[a-z]/g)

        //If input string doesn't contain any english letters,
        //return without storing input to API
        if (charsToBeTranslated === null) {
            setToBeTranslated([])
            setApiLoading(false)
            return
        }

        //Storing rawInput to API, because the string is probably more 
        //readable before whitespaces and special characters are removed
        const [data, errorMsg] = await addTranslationToAPI(user, rawInput)
        
        //check for error before updating store and local storage
        if (errorMsg != null) {
            console.log(errorMsg)
            window.alert("Could not store translation to profile.")
        }
        else {
            window.localStorage.setItem("username", JSON.stringify(data))
            dispatch(setUser(data))
        }

        //update local state with char array that shall be translated to sign language
        setToBeTranslated(charsToBeTranslated)

        setApiLoading(false)
    }
    

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)}>
                <label>Text to be translated to sign language: </label>
                <input type="text" style={{width: "250px"}}
                    placeholder="Maximum 40 characters"
                    {...register("rawInput", {maxLength: 40 })} />

                {/* Displaying text to inform the user if the input doesn't meet the requirements */}
                {errors.rawInput && (
                    (errors.rawInput.type === "maxLength" && <span>Maximum 40 characters</span>)
                )}

                {/* during api requests: Button is disabled and message is displayed */}
                <button type="submit" disabled={apiLoading}>Translate</button>
                {apiLoading && <p>Translating...</p>}
            </form>
            
            {/* Render sign language output*/}
            <TranslationOutput data={toBeTranslated} />
        </>
        
    )
}

export default TranslationInput