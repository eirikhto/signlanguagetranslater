import { useDispatch, useSelector } from "react-redux"
import { Navigate } from "react-router-dom"
import { setUser } from "../redux-parts/userSlice"


//guard that redirects to login page if user is not logged in
const withAuth = Component => props => {

    //global state
    const user = useSelector(state => state.user.value)
    const dispatch = useDispatch()

    const locallyStoredUser = window.localStorage.getItem("username")

    //Check if user is logged in before displaying page
    if (locallyStoredUser !== null) {
        //if store is reset to inital value, 
        //update the store with data from the local storage
        if (user === null) {
            dispatch(setUser(JSON.parse(locallyStoredUser)))
        }
        return <Component {...props}/>
    }
    else {
        return <Navigate to="/"/> //navigate to login page
    }
}

export default withAuth