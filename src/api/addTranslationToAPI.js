import { createHeaders } from "./index"
const apiUrl = process.env.REACT_APP_API_URL

//appends translation to user data in API
export const addTranslationToAPI = async (user, translation) => {
    try {
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [...user.translations, translation]
            })
        })
        if (!response.ok) {
            throw new Error("Could not save new translation to API")
        }
        const data = await response.json()
        return [data, null]
    }
    catch (error){
        return [null, error.message]
    }
}