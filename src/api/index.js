const apiKey = process.env.REACT_APP_API_KEY

//Creates headers for use when sending requests to API
export  const createHeaders = () => {
    return {
        'Content-Type': 'application/json',
        'x-api-key': apiKey
    }
}