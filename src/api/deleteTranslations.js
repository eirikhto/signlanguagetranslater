import { createHeaders } from "./index"
const apiUrl = process.env.REACT_APP_API_URL

//deletes the users translation from the api 
export const deleteTranslations = async (user) => {
    try {
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: []
            })
        })
        if (!response.ok) {
            throw new Error("Could not delete translations")
        }
        const data = await response.json()
        return [data, null]
    }
    catch (error){
        return [null, error.message]
    }
}