import { createHeaders } from "./index"
const apiUrl = process.env.REACT_APP_API_URL

//fetches user from API
//returns array with first entry representing user data, 
//and second entry representing error
const getUser = async (username) => {
    try {
        const response = await fetch(`${apiUrl}?username=${username}`)
        if (!response.ok) {
            throw new Error("Could not fetch data from API")
        }
        const data = await response.json()
        return [data, null]
    }
    catch (error){
        return [null, error.message]
    }
}

//creates new user and posts it to the API
//returns array with first entry representing user data, 
//and second entry representing error message
const createNewUser = async (username) => {
    try {
        const response = await fetch(apiUrl, {
            method: 'POST',
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        })
        if (!response.ok) {
            throw new Error("Could not post new user to API")
        }
        const data = await response.json()
        return [data, null]
    }
    catch (error){
        return [null, error.message]
    }
}


//takes username as input and returns user data and/or an error message
//creates new user if user does not already exist in API
export const loginUser = async (username) => {
    //looks for user in API
    const [data, errorMsg] = await getUser(username)
    if (errorMsg !== null) {
        //some error occured when fetcing data from API
        return [data, errorMsg]
    }
    if (data.length === 0) {
        //User does not exist in API. Create new user
        return await createNewUser(username)
    }
    return [data[0], errorMsg]
}