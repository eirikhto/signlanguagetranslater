import { createSlice } from "@reduxjs/toolkit";

/* React redux. Creating slice with reducer */

export const userSlice = createSlice({
    name: 'user',
    initialState: {
        value: null,
    },
    reducers: {
        setUser: (state, action) => {
            state.value = action.payload
        }
    }
})

export const { setUser } = userSlice.actions

export default userSlice.reducer