import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./userSlice"

//react redux store setup
export default configureStore({
    reducer: {
        user: userReducer,
    },
})